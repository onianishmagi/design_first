
$(document).ready(function(){
    $(window).scroll(function () {
        var scroll = $(window).scrollTop();
        if(scroll > 0){
            $("#nav").addClass("nav-fixed");
            // console.log(scroll);
        }else if(scroll < 90){
            $("#nav").removeClass("nav-fixed");
        }

        var pportfolio = Math.floor($("#portfolio").offset().top);
        var pabout = Math.floor($("#about").offset().top);
        var pcontact = Math.floor($("#contact").offset().top);
        // console.log('portfolio: ' + pportfolio+ "----about: " + pabout+ "-----contact: " + pcontact);
        // console.log(scroll);


        if (scroll >= 0 && scroll < pportfolio){
            $("#nav-home").addClass("nav-active");
            $("#nav-portfolio").removeClass("nav-active");
        }else if (scroll >= pportfolio  && scroll < pabout){
            $("#nav-home").removeClass("nav-active");
            $("#nav-about").removeClass("nav-active");
            $("#nav-portfolio").addClass("nav-active");
        }else if (scroll >= pabout && scroll < pcontact){
            $("#nav-portfolio").removeClass("nav-active");
            $("#nav-contact").removeClass("nav-active");
            $("#nav-about").addClass("nav-active");
        }else if (scroll >= pcontact){
            $("#nav-about").removeClass("nav-active");
            $("#nav-contact").addClass("nav-active");
        }


    });
});

